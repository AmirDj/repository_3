"use strict";

"use strict";

const drawButton = document.getElementById("drawButton");
const inputContainer = document.createElement("div");
const diameterInput = document.createElement("input");
const submitButton = document.createElement("button");
const circleContainer = document.createElement("div");

drawButton.addEventListener("click", () => {
  inputContainer.style.display = "block";
});

submitButton.addEventListener("click", () => {
  const circleDiameter = parseFloat(diameterInput.value);
  if (!isNaN(circleDiameter)) {
    drawCircles(circleDiameter);
    inputContainer.style.display = "none";
  }
});

function drawCircles(circleDiameter) {
  circleContainer.innerHTML = "";
  for (let i = 0; i < 100; i++) {
    const circle = document.createElement("div");
    circle.classList.add("circle");
    Object.assign(circle.style, {
      width: `${circleDiameter}px`,
      height: `${circleDiameter}px`,
      backgroundColor: getRandomColor()
    });
    circleContainer.appendChild(circle);

    circle.addEventListener("click", () => {
      circle.remove();
      const circles = Array.from(document.querySelectorAll(".circle"));
      circles.forEach((c) => {
        if (c.offsetTop === circle.offsetTop && c.offsetLeft > circle.offsetLeft) {
          c.style.left = `${parseInt(c.style.left, 10) - circleDiameter - 5}px`;
        }
      });
    });
  }
}

function getRandomColor() {
  const letters = "0123456789ABCDEF";
  let color = "#";
  for (let i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

inputContainer.id = "inputContainer";
inputContainer.style.display = "none";

Object.assign(diameterInput, {
  type: "number",
  id: "diameterInput",
  placeholder: "Діаметр кола",
  style: "margin-right: 10px"
});

Object.assign(submitButton, {
  id: "submitButton",
  textContent: "Намалювати"
});

Object.assign(circleContainer.style, {
  display: "grid",
  gridTemplateColumns: "repeat(10, 1fr)",
  gap: "5px"
});

inputContainer.append(diameterInput, submitButton);
document.body.append(drawButton, inputContainer, circleContainer);
