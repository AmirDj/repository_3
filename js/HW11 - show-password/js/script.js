"use strict";

//Завдання

const inputPass = document.querySelector('#inputPass');
const iconInput = inputPass.nextElementSibling;
const confirmPass = document.querySelector('#confirmPass');
const iconConfirm = confirmPass.nextElementSibling;
const submitBtn = document.querySelector('#submitBtn');
const passForm = document.querySelector("#passForm");

function onIconClick(inputElem) {
    if(inputElem.type === "password"){
        inputElem.type = "text";
        inputElem.nextElementSibling.classList.remove("fa-eye");
        inputElem.nextElementSibling.classList.add("fa-eye-slash");
    }else{
        inputElem.type = "password";
        inputElem.nextElementSibling.classList.remove("fa-eye-slash");
        inputElem.nextElementSibling.classList.add("fa-eye");
    }
}

function checkPass() {
    const inputPassVal = inputPass.value;
    const confirmPassVal = confirmPass.value;

    if (inputPassVal === confirmPassVal) {
        removeErrorMessage();
        alert("You are welcome");
    }else{
        const errorMessage = document.createElement("p");
        errorMessage.textContent = "Потрібно ввести однакові значення";
        errorMessage.style.color = "red";
        errorMessage.classList.add("error-message");
        passForm.append(errorMessage);
    }
}

function removeErrorMessage() {
    const errorMessage = document.querySelectorAll(".error-message");
    if (errorMessage) {
        errorMessage.forEach(element => element.remove());
    }
}

iconInput.addEventListener("click", () => {
    onIconClick(inputPass);
});
iconConfirm.addEventListener("click", () => {
    onIconClick(confirmPass);
});
submitBtn.addEventListener("click", () => {
    checkPass();
});