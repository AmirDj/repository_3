"use strict";

//Теоретичні питання
/*
№1
Для вводу чогось в поле <input> є багато способів, зокрема копіювання/вставка за допомогою миші або розпізнавання мови. 
Введення даних відбувається без використання клавіатури, отже події клавіатури не генеруються, і їх використання 
для відстеження даних, введених в <input>, нам нічого не дасть.

*/

//Завдання

const buttons = document.querySelectorAll('.btn');
const validKeys = [...buttons].map(btn => btn.dataset.key.toUpperCase());

document.addEventListener('keydown', (event) => {
    const key = event.key.toUpperCase();
    if (validKeys.includes(key)) {
        buttons.forEach(btn => {
            if (btn.dataset.key.toUpperCase() === key) {
                btn.classList.add('active');
            } else {
                btn.classList.remove('active');
            }
        });
    }
});
