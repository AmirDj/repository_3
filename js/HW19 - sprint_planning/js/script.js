"use strict";

function calculateProjectTimeline(teamSpeeds, backlog, deadline) {
    const workHoursPerDay = 8;
    const millisecondsPerDay = 24 * 60 * 60 * 1000;

    const totalTeamSpeed = teamSpeeds.reduce((total, speed) => total + speed, 0);
    const totalBacklogPoints = backlog.reduce((total, points) => total + points, 0);

    let currentDate = new Date();
    let workDaysLeft = 0;

    while (currentDate < deadline) {
        if (currentDate.getDay() !== 0 && currentDate.getDay() !== 6) {
            workDaysLeft++;
        }
        currentDate = new Date(currentDate.getTime() + millisecondsPerDay);
    }

    const requiredWorkDays = Math.ceil(totalBacklogPoints / totalTeamSpeed);

    if (workDaysLeft >= requiredWorkDays) {
        const daysToDeadline = Math.min(workDaysLeft, requiredWorkDays);
        console.log(`Усі завдання будуть успішно виконані за ${daysToDeadline} днів до настання дедлайну!`);
    } else {
        const extraWorkDays = requiredWorkDays - workDaysLeft;
        const extraWorkHours = extraWorkDays * workHoursPerDay;
        console.log(`Команді розробників доведеться витратити додатково ${extraWorkHours} годин після дедлайну, щоб виконати всі завдання в беклозі`);
    }
}

const teamSpeeds = [7, 2, 4, 5, 2]; 
const backlog = [2, 1, 3, 5, 6, 4, 7]; 
const deadline = new Date('2023-01-28'); 

calculateProjectTimeline(teamSpeeds, backlog, deadline);