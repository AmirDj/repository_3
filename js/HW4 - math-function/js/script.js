"use strict";

//Теоретичні питання
/*
 №1 

 Функції дозволяють згрупувати певний блок коду і дати йому ім'я. Це дозволяє використовувати цей код в інших частинах програми без необхідності повторного його написання.

 №2

 Передача аргументів у функцію є способом надати функції необхідні дані або вхідні параметри для виконання певних операцій. 
 Передача аргументів у функцію дозволяє функції працювати зі специфічними даними та забезпечує більшу загальність, гнучкість та перевикористовування коду

 №3

 Оператор "return" використовується у функціях для повернення значення з функції назад у викликаючий код. Коли функція досягає оператора, вона завершує своє виконання і повертає значення, яке було вказане після оператора. Це значення може бути використане в інших частинах програми або присвоєне змінній для подальшої обробки. Оператор "return" також може використовуватися для негайного завершення функції, якщо його викликати без значення.
*/

Завдання

function mathOperation(firstNum, op, secondNum){
    switch (op) {
        case "+":
            return firstNum + secondNum;
            break;

        case "-":
            return firstNum - secondNum;
            break;

        case "*":
            return firstNum * secondNum;
            break;

        case "/":
            return firstNum / secondNum;
            break;
            
        default:
            console.log("You entered an invalid operator.");
            break;
    }
}

let firstNumber = Number(prompt("Enter first number"));
let operator = prompt("Enter an operator: +, -, *, /");
let secondNumber = Number(prompt("Enter second  number"));

console.log(mathOperation(firstNumber, operator, secondNumber));


