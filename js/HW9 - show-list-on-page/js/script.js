"use strict";

//Теоретичні питання
/*
№1
Можна використати метод document.createElement(tag).

№2
elem.insertAdjacentHTML(pos, html);
Перший параметр вказує на те куди ми зочимо вставити html відносно elem.
"beforebegin" – вставити html перед elem,
"afterbegin" – вставити html в elem, на початку,
"beforeend" – вставити html в elem, в кінці,
"afterend" – вставити html після elem.

№3
Можна використати метод element.remove().

*/

//Завдання

function createList(arr, parent = document.body) {
    const ul = document.createElement("ul");

    arr.forEach(item => {
        const li = document.createElement("li");
        if (Array.isArray(item)) {
          createList(item, li);
        } else {
          li.textContent = item;
        }
        ul.append(li);
      });

    parent.prepend(ul);
}

const array1 = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const array2 = ["1", "2", "3", "sea", "user", 23];
const array3 = [2222, "Kharkiv", "Kiev", 25n, true, false, ["Borispol", "Irpin", function () {}, alert], "Odessa", "Lviv", "Dnieper"];

const parent = document.querySelector("#listParent");
createList(array1, parent);
createList(array2);
createList(array3, parent);

function clearPage() {
    const countdownElement = document.createElement('div');
    countdownElement.textContent = '3';
    document.body.append(countdownElement);

    let countdown = 3;
    const countdownInterval = setInterval(() => {
        countdown--;
        countdownElement.textContent = `${countdown}`;
        if (countdown <= 0) {
            clearInterval(countdownInterval);
            document.body.innerHTML = ''; 
        }
    }, 1000);
}

setTimeout(clearPage, 3000);