"use strict"

//Завдання
// Отримуємо посилання на табло та кнопки
const display = document.querySelector('.display input');
const buttons = document.querySelectorAll('.button');

// Ініціалізуємо змінні для обробки операцій та чисел
let currentInput = '';
let currentOperator = '';
let memory = 0;

// Функція для обробки кліків на кнопки
function handleButtonClick(event) {
  const value = event.target.value;

  if (isNumeric(value)) {
    // Якщо натиснута цифра або крапка, додаємо її до поточного вводу
    currentInput += value;
  } else if (value === 'C') {
    // Якщо натиснута "C", очищаємо всі дані
    currentInput = '';
    currentOperator = '';
  } else if (value === '=') {
    // Якщо натиснута "=", обчислюємо результат
    const secondNumber = parseFloat(currentInput);
    if (!isNaN(secondNumber)) {
      currentInput = calculateResult(memory, currentOperator, secondNumber);
      currentOperator = '';
      memory = 0;
    }
  } else if (value === 'm+') {
    // Якщо натиснута "m+", додаємо число до пам'яті
    const num = parseFloat(currentInput);
    if (!isNaN(num)) {
      memory += num;
    }
  } else if (value === 'm-') {
    // Якщо натиснута "m-", віднімаємо число з пам'яті
    const num = parseFloat(currentInput);
    if (!isNaN(num)) {
      memory -= num;
    }
  } else if (['+', '-', '*', '/'].includes(value)) {
    // Якщо натиснута операція (+, -, *, /), зберігаємо її та перший операнд
    currentOperator = value;
    memory = parseFloat(currentInput);
    currentInput = '';
  }

  // Оновлюємо вміст табло
  display.value = currentInput;
}

// Функція для перевірки, чи є значення числом
function isNumeric(value) {
  return !isNaN(parseFloat(value)) && isFinite(value);
}

// Функція для обчислення результату
function calculateResult(firstNumber, operator, secondNumber) {
  switch (operator) {
    case '+':
      return (firstNumber + secondNumber).toString();
    case '-':
      return (firstNumber - secondNumber).toString();
    case '*':
      return (firstNumber * secondNumber).toString();
    case '/':
      if (secondNumber === 0) {
        return 'Error';
      }
      return (firstNumber / secondNumber).toString();
    default:
      return '';
  }
}

// Додаємо обробчик кліків на кнопки
buttons.forEach(button => {
  button.addEventListener('click', handleButtonClick);
});

// Додаємо можливість використовувати клавішу Enter для обчислення
document.addEventListener('keydown', event => {
  if (event.key === 'Enter') {
    const equalsButton = document.querySelector('.button.orange');
    equalsButton.click();
  }
});
