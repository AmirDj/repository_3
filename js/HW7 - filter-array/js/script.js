"use strict";

//Теоретичні питання
/*

№1
Метод forEach ітерується по кожному елементу масиву і передає його 
як аргумент у callback функцію для, яка виконується один раз для кожного елемента масиву.

№2
Найпростіший спосіб це присвоїти властивості "length" значення 0. (arr.length = 0). 

№3
Використати метод "Array.isArray()".

*/

//Завдання

function filterBy(arr, dataType) {
    return arr.filter(function (element) {
        if ((dataType === "object" && typeof element === "function") || (dataType ==="null" && element === null)) {
            return false;
        }else if (dataType === "object" && element === null) {
            return true
        }
        
        return typeof element !== dataType;
    });
}

const arr = ['hello', 23, '23', null, [], {}, function fan(){ }, undefined, true, false, Symbol("id"), 5n, alert];
const dataType = "object";

let result = filterBy(arr, dataType);
console.log(result);