"use strict";

//Теоретичні питання

/*

№1
DOM - структура HTML сторінки у вигляді об'єктів.

№2
innerHTML працює з працює з HTML-кодом, включаючи теги та їх вміст. 
innerText працює лише з текстом, ігноруючи HTML-структури.

№3
Щоб звернутися до елемента сторінки за допомогою JavaScript, 
спочатку потрібно його знайти. Існує 6 основних методів пошуку елементів:
- getElementById - пошук за атрибутом "id";
- getElementsByName - пошук за атрибутом "name";
- getElementsByTagName - пошук елементів за тегом;
- getElementsByClassName - пошук елементів за СSS-класом;
- querySelector - пошук першого елемента який відповідає CSS-селектору;
- querySelectorAll - пошук всіх елементів, що відповідають заданому CSS-селектору.
Найкращим способом є використання методів querySelector або querySelectorAll, 
оскільки вони потужніші і коротші для написання Вони є більш гнучкими та широко 
підтримуються в сучасних браузерах..
Методи getElementsBy... можуть бути використані, але вони зазвичай 
розглядаються як історичні або менш потужні методи.

*/

//Завдання

let paragraphs = document.querySelectorAll("p");
for (const paragraph of paragraphs) {
    paragraph.style.background = "#ff0000"; 
}

let optionsList = document.querySelector("#optionsList");
console.log(optionsList);

let parentElem = optionsList.parentElement;
console.log(parentElem);

if (optionsList.hasChildNodes()) {
  for (const node of optionsList.childNodes) {
    console.log('Name:', node.nodeName, 'Type:', node.nodeType);
  }
}

let testParagraph = document.querySelector("#testParagraph");
testParagraph.textContent = "This is a paragraph";

const nestedElements = document.querySelectorAll('.main-header *');

nestedElements.forEach((element) => {
  element.classList.add("nav-item");
  console.log(element);
});

let sectionTitle = document.querySelectorAll(".section-title");
sectionTitle.forEach(element => {
  element.classList.remove("section-title");
});