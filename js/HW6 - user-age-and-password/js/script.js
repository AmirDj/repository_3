"use strict";

//Теоретичні питання
/*

№1

Екранування - це процес використання спеціальних символів або послідовностей символів для зміни поведінки 
або інтерпретації інших символів у рядках коду програми. Перед спеціальними символами треба ставити бекслеш "\", 
для того щоб вони сприймались як рядки.

№2

Function declaration, function expression та стрілочні функції.

№3

Hoisting - це можливість отримання доступу до змінних та функцій до їх фактичного використання. 
Змінні та функції піднімаються вгору по своїй області видимості. Це стосується 
лише їх оголошення, а не ініціалізації або присвоєння значення.

*/

//Завдання

function createNewUser() {
    const newUser = {};

    newUser.firstName = prompt("Enter your first name:");
    newUser.lastName = prompt("Enter your last name:");
    newUser.birthday = prompt("Enter your date of birth (dd.mm.yyyy):");
    newUser.getLogin = function (){
        return `${(newUser.firstName.charAt(0) + newUser.lastName).toLowerCase()}`;
    };
    newUser.getAge = function () {
        const today = new Date();
        const birthDate = new Date(
            +newUser.birthday.slice(6),
            +newUser.birthday.slice(3, 5) - 1,
            +newUser.birthday.slice(0, 2)
        );

        let age = today.getFullYear() - birthDate.getFullYear();
        
        const hasBirthdayPassed = (
            today.getMonth() > birthDate.getMonth() || (today.getMonth() === birthDate.getMonth() && today.getDate() >= birthDate.getDate())
        );
        
        if (!hasBirthdayPassed) {
            age--;
        }

        return age;
    }
    newUser.getPassword = function () {
        return `${newUser.firstName[0].toUpperCase() + newUser.lastName.toLowerCase() + newUser.birthday.slice(6)}`;
    }
    return newUser;
}

const user = createNewUser();
console.log(user);
console.log(user.getAge());
console.log(user.getPassword());


  