"use strict";

//Теоретичні питання
/*

№1

Метод об'єкту - це функція яка є властивістю об'єкта. 

№2

Значенням властивості об'єкта може бути будь-який тип даних.

№3

Посилальний тип даних є способом зберігання та передачі значень в програмі. 
Він зберігає у собі не значення об'єкта, а посилання на його місцезнаходження в пам'яті.


*/

//Завдання

function createNewUser() {
    const newUser = {};

    newUser.firstName = prompt("Enter your first name:");
    newUser.lastName = prompt("Enter your last name:");
    newUser.getLogin = function (){
        return `${(newUser.firstName.charAt(0) + newUser.lastName).toLowerCase()}`;
    };

    return newUser;
}

const user = createNewUser();
console.log(user.getLogin());
