"use strict";

//Теоретичні питання
/*
№1

Оголосити змінну у Javascript можна за допомогою ключових слів "let", "const" та "var". 
Використання "var" є застарілим і неправильним.
Після ключового слова треба написати ім'я для змінної яке б відображало зміст інформації, що буде зберігатись у цій змінній.   

№2

Метод prompt використовується коли ми хочемо задати питання користувачу, а він передав нам які-небудь дані. 
При його використанні з'являється модальне вікно з питанням, місцем для введення даних та двома кнопками: ОК та Скасувати.
Якщо користувач ввів дані та натиснув ОК то вони повернуться у вигляді рядка(string). Користувач може нічого не ввести і натиснути
ОК тоді повернеться порожній рядок(<empty string>). Користувач може нічого не ввести і натиснути Скасувати тоді повернеться 
нічого(null). Також, біля нашого питання, можна ввести необов’язковий другий параметр який виводитиме значення за замовчуванням.

Метод confirm використовується коли ми хочемо запитати у користувача його думку. При його використанні з'являється 
модальне вікно з питанням та двома кнопками: ОК та Скасувати. При натисканні ОК повертаєтья значення true, 
а при натисканні Скасувати значення false.   

№3

Неявне перетворення типів це автоматична конвертація типів даних змінних. Здебільшого таке буває при використанні операторів для роботи
з різними типами даних.
Наприклад:

let str = "7";  рядок
let obj = {};   об'єкт

console.log(str + obj); 
Ми спробували скласти рядок та об'єкт. Результатом став рядок "7[object Object]". Об'єкт перетворився у рядок 
і додався до вже існуючого рядка, хоч ми й не мали намірів перетворювати об'єкт на рядок, тобто сталося неявне перетворення типів. 
*/

//Завдання
//№1
const name = "Amir";
const admin = name;
console.log(admin);

//№2
let days = 7;
const oneDay = 86400;
let secondsInDays = days * oneDay;
console.log(secondsInDays);

//№3
console.log(prompt("Enter value"));
