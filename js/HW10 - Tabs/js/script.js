"use strict";

//Завдання

const tabs = document.querySelectorAll(".tabs-title");
const tabContents = document.querySelectorAll(".tab");

function showTab(currentTab) {
    tabContents.forEach((tab) => {
        if (tab.getAttribute("data-tab-name") === currentTab) {
            tab.classList.add("active");
        } else {
            tab.classList.remove("active");
        }
    });
}

function setActiveTab(event) {
    tabs.forEach((tab) => {
    tab.classList.remove("active");
});
    let currentTab = event.target;
    currentTab.classList.add("active");
    let tabName = currentTab.getAttribute("data-tab-name");
    showTab(tabName);
}
tabs.forEach((tab) => tab.addEventListener("click", setActiveTab));