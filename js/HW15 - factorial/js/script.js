"use strict";

//Теоретичні питання
/*

№1
Рекурсія - це коли функція викликає саму себе.
Рекурсія корисна коли завдання може бути розділене на кілька завдань того ж роду, але простіших.
Рекурсія може дати коротший код, який легше зрозуміти та підтримувати.

*/

//Завдання

let userNum = prompt("Enter a number:");

if (userNum.trim() === "" || userNum === null || Number.isNaN(Number(userNum))) {
    userNum = prompt("Error, please try again:", userNum)
}else{
    alert(factorial(Number(userNum)));
}

function factorial(num) {
    return num === 0 ? 1 : num * factorial(num - 1);
}
