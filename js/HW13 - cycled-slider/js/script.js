"use strict";

//Теоретичні питання
/*
№1
setTimeout() виконує функцію один раз через встановлений період часу.
setInterval() виконує функцію постійно через встановлений інтервал часу. 

№2
Вона спрацює не миттєво і тільки після завершення виконання поточного коду. 
Виконання функцій у JS відбувається у вигляді черги подій (event loop) і 
у програмі можуть бути функції з більш високим пріорітетом або код, який виконується в даний момент.
Ці події завжди будуть перевірятись перед виконанням setTimeout().

№3
Для того, щоб цикл не споживав ресурси ЦП та пам'яті, не навантажував браузер 
та не призводив до зниження продуктивності програми.

*/
//Завдання

const images = document.querySelectorAll(".image-to-show");
const stopButton = document.querySelector("#stopButton");
const resumeButton = document.querySelector("#resumeButton");
const timerDisplay = document.querySelector("#timer");
let currentIndex = 0;
let timer;
let isSlideShowRunning;
let startTime = 0;

function showImage(index) {
    images.forEach((image, ind) => {
        image.style.display = "none";
        if (ind === index) {
            fadeIn(image);
        } else {
            fadeOut(image);
        }
    });
}

function fadeIn(image) {
    let opacity = 0;
    image.style.display = "block";
    image.style.opacity = 0;

    const fadeInInterval = setInterval(() => {
        opacity += 0.05;
        image.style.opacity = opacity;
        if (opacity >= 1) {
            clearInterval(fadeInInterval);
        }
    });
}

function fadeOut(image) {
    let opacity = 1;

    const fadeOutInterval = setInterval(() => {
        opacity -= 0.05;
        image.style.opacity = opacity;
        if (opacity <= 0) {
            image.style.display = "none";
            clearInterval(fadeOutInterval);
        }
    });
}

function showNextImage() {
    currentIndex = (currentIndex + 1) % images.length;
    showImage(currentIndex);
}

function updateTimer() {
    const passed = Date.now() - startTime;
    const secondsLeft = (3000 - passed) / 1000;
    timerDisplay.textContent = `До наступної картинки: ${secondsLeft.toFixed(1)} секунд`;
    if (secondsLeft <= 0) {
        showNextImage();
        startTime = Date.now();
    }
}

function startSlideShow() {
    if (!isSlideShowRunning) {
        isSlideShowRunning = true;
        startTime = Date.now();

        timer = setInterval(updateTimer, 10);
    }
}

function stopSlideShow() {
    if (isSlideShowRunning) {
        isSlideShowRunning = false;
        clearInterval(timer);
        timerDisplay.textContent = `До наступної картинки: 0 секунд`;
    }
}

stopButton.addEventListener("click", () => {
    stopSlideShow();
});

resumeButton.addEventListener("click", () => {
    startSlideShow();
});

showImage(currentIndex);
startSlideShow();
