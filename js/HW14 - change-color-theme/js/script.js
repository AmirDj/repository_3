"use strict";

//Завдання

function onBtnCkick() {
    const theme = localStorage.getItem("theme");

    if (theme === "dark") {
        document.body.classList.remove("dark-theme");
        localStorage.setItem("theme", "light");
    } else {
        document.body.classList.add("dark-theme");
        localStorage.setItem("theme", "dark");
    }
}

const theme = localStorage.getItem("theme");
if (theme === "dark") {
    document.body.classList.add("dark-theme");
}

const buttonTheme = document.querySelector("#theme-button");
buttonTheme.addEventListener("click", onBtnCkick);
