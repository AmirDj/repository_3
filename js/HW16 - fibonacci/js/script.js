"use strict";

//Завдання

const n = +prompt("Enter the value:");
const firstNum = 3;
const secondNum = 4;

alert(fibonacciNum(firstNum, secondNum, n))

function fibonacciNum(first, second, n) {
    if(n === 0){
        return first;
    }
     if (n === 1) {
        return second;
    }

    let prevFirst = first;
    let prevSecond = second;
    let result;

    if(n < 0){
        for (let i = 2; i <= -n; i++) {
            result = prevFirst - prevSecond;
            prevFirst = prevSecond;
            prevSecond = result;
        }
    }else{
        for (let i = 2; i <= n; i++) {
            result = prevFirst + prevSecond;
            prevFirst = prevSecond 
            prevSecond = result;
        }
    }

    return result;
}