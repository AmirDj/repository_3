'use strict';

//#region General

const active = 'active';
const loaderWrapper = document.querySelector('.loader-wrapper');

function clearActive(items) {
  [...items].forEach((item) => {
    item.classList.remove(active);
  });
}

function setActive(item) {
  item.classList.add(active);
}

function getActive(items) {
  return [...items].find((item) => item.classList.contains(active));
}

//#endregion

//#region section 'Our Services'

const tabsTitle = 'service-tab-title';
const tabsMenu = document.querySelector('.services-tabs');
const tabsContent = document.querySelector('.services-tabs-content');

tabsMenu.addEventListener('click', menuToggleActive);

function menuToggleActive(e) {
  const item = e.target;
  if (item.classList.contains(tabsTitle) && !item.classList.contains(active)) {
    clearActive(tabsMenu.children);
    setActive(item);
    contentToggleActive(item);
  }
}

function contentToggleActive(item) {
  const contentItem = connectTabsContent(item);

  if (contentItem) {
    clearActive(tabsContent.children);
    setActive(contentItem);
  }
}

function connectTabsContent(item) {
  for (let i of tabsContent.children) {
    if (i.dataset.serviceItem === item.dataset.serviceItem) {
      return i;
    }
  }
  return false;
}

//#endregion section 'Our Services'

//#region section 'Work'
const imagesMenuItem = 'work-filter-item';
const countItemsContent = [12,24];
const maxCountPushLoadBtn = 1;
let currentCountPushLoadBtn = 0;

const imagesMenu = document.querySelector('.work-filter');
const imagesContent = document.querySelector('.work-images-content');
const imagesLoadBtn = document.querySelector('.work-button');

const all = {
  textForFirstItem: 'All Projects',
  datasetValue: 'all',
};
const graphicDesign = {
  textForFirstItem: 'Graphic Design',
  datasetValue: 'graphic_design',
  path: './img/section_work/graphic_design/',
  items: [
    'graphic-design1.jpg',
    'graphic-design2.jpg',
    'graphic-design3.jpg',
    'graphic-design4.jpg',
    'graphic-design5.jpg',
    'graphic-design6.jpg',
    'graphic-design7.jpg',
    'graphic-design8.jpg',
    'graphic-design9.jpg',
    'graphic-design10.jpg',
    'graphic-design11.jpg',
    'graphic-design12.jpg',
  ],
};
const webDesign = {
  textForFirstItem: 'Web Design',
  datasetValue: 'web_design',
  path: './img/section_work/web_design/',
  items: [
    'web-design1.jpg',
    'web-design2.jpg',
    'web-design3.jpg',
    'web-design4.jpg',
    'web-design5.jpg',
    'web-design6.jpg',
    'web-design7.jpg',
    'web-design8.jpg',
    'web-design9.jpg',
    'web-design10.jpg',
    'web-design11.jpg',
    'web-design12.jpg',
  ],
};
const landingPages = {
  textForFirstItem: 'Landing Pages',
  datasetValue: 'landing_pages',
  path: './img/section_work/landing_page/',
  items: [
    'landing-page1.jpg',
    'landing-page2.jpg',
    'landing-page3.jpg',
    'landing-page4.jpg',
    'landing-page5.jpg',
    'landing-page6.jpg',
    'landing-page7.jpg',
    'landing-page8.jpg',
    'landing-page9.jpg',
    'landing-page10.jpg',
    'landing-page11.jpg',
    'landing-page12.jpg',
  ],
};
const wordpress = {
  textForFirstItem: 'Wordpress',
  datasetValue: 'wordpress',
  path: './img/section_work/wordpress/',
  items: [
    'wordpress1.jpg',
    'wordpress2.jpg',
    'wordpress3.jpg',
    'wordpress4.jpg',
    'wordpress5.jpg',
    'wordpress6.jpg',
    'wordpress7.jpg',
    'wordpress8.jpg',
    'wordpress9.jpg',
    'wordpress10.jpg',
    'wordpress11.jpg',
    'wordpress12.jpg',
  ],
};

function handlerClickTab(e) {
  const item = e.target;

  if (
    item.classList.contains(imagesMenuItem) &&
    !item.classList.contains(active)
  ) {
    clearActive(imagesMenu.children);
    setActive(item);
    changeImagesContent();
  }
}

function handlerLoadBtn() {
  loaderWrapper.classList.add('enable');
  setTimeout(() => {
    loaderWrapper.style.opacity = 1;
  }, 300);

  setTimeout(() => {
    setTimeout(() => {
      loaderWrapper.classList.remove('enable');
    }, 1000);
    loaderWrapper.style.opacity = 0;

    currentCountPushLoadBtn++;

    changeImagesContent();

    if (currentCountPushLoadBtn === maxCountPushLoadBtn) {
      imagesLoadBtn.classList.add('disable');
    }
  }, 2000);
}

function changeImagesContent() {
  const imagesMenuItemActive = getActive(imagesMenu.children);
  const nameObjItems = getObjNameFromDataset(imagesMenuItemActive);

  if (nameObjItems) {
    let i = 0;
    let countImage = 0;

    clearImagesContent();

    for ( let count = 0; count < countItemsContent[currentCountPushLoadBtn]; count++) {
      i = Math.floor(Math.random() * nameObjItems.length);

      imagesContent.insertAdjacentHTML(
        'beforeend',
        imageItemContent(
          nameObjItems[i].textForFirstItem,
          nameObjItems[i].datasetValue,
          nameObjItems[i].path,
          nameObjItems[i].items[countImage]
        )
      );
      countImage =
        countImage >= nameObjItems[i].items.length - 1 ? 0 : countImage + 1;
    }
  } else {
    alert('Error: not find nameObjItems');
  }
}

function clearImagesContent() {
  imagesContent.innerHTML = '';
}

function imageItemContent(text, datasetValue, path, name) {
  return `
  <div class="work-images-item" data-work-filter-category="${datasetValue}">
    <div class="work-images-item front">
      <div class="work-images-item-img">
        <img src="${path}${name}" alt="${text}" />
      </div>
    </div>

    <div class="work-images-item back">
      <div class="work-content-item-text">
        <ul class="work-text-circles">
          <li class="button-circle-white">
            <i class="fa-sharp fa-solid fa-link"></i>
          </li>
          <li class="button-circle-green">
            <i class="fa-sharp fa-solid fa-square"></i>
          </li>
        </ul>
        <div class="work-text-title text-green">
          creative design
        </div>
        <div class="work-text-title-subtitle">${text}</div>
      </div>
    </div>
  </div>
  `;
}

function getObjNameFromDataset(item) {
  let result;

  switch (item.dataset.workFilterCategory) {
    case 'all':
      result = [graphicDesign, webDesign, landingPages, wordpress];
      break;
    case 'graphic_design':
      result = [graphicDesign];
      break;
    case 'web_design':
      result = [webDesign];
      break;
    case 'landing_pages':
      result = [landingPages];
      break;
    case 'wordpress':
      result = [wordpress];
      break;
    default:
      result = false;
  }
  return result;
}



imagesMenu.addEventListener('click', handlerClickTab);

if (currentCountPushLoadBtn < maxCountPushLoadBtn) {
  imagesLoadBtn.addEventListener('click', handlerLoadBtn);
}else{
  imagesLoadBtn.classList.add('disable');
}

changeImagesContent();

//#endregion section 'Work'

//#region section 'Feedback'

const carouselItemsPhoto = document.querySelector('.carousel-foto-content');
const itemPhotoAll = document.querySelectorAll(
  '.carousel-foto-content .item-foto'
);
const sliderItems = document.querySelector('.slider-content');
const prevBtn = document.querySelector('.slider-left');
const nextBtn = document.querySelector('.slider-right');

const carouselPhotoArray = [];
let indexCarouselPhotoActive;
const indexesCarouselPhotos = [];
const countAddPhotoCarousel = 4;
const valuePxMove = 80;
const valuePxDefault = -160;

function createSlider() {
  for (let i = 0; i < itemPhotoAll.length; i++) {
    carouselPhotoArray.push(itemPhotoAll[i]);
    if (itemPhotoAll[i].classList.contains(active)) {
      indexCarouselPhotoActive = i;
    }
    itemPhotoAll[i].remove();
  }
  setIndexesCarouselPhotos();
}

function setIndexesCarouselPhotos() {
  indexesCarouselPhotos.length = 0;
  carouselItemsPhoto.innerHTML = '';
  for (
    let i = indexCarouselPhotoActive - countAddPhotoCarousel;
    i <= indexCarouselPhotoActive + countAddPhotoCarousel;
    i++
  ) {
    let item;
    if (i < 0) {
      item = carouselPhotoArray.length + i;
    }
    if (i >= 0 && i < carouselPhotoArray.length) {
      item = i;
    }
    if (i >= carouselPhotoArray.length) {
      item = i - carouselPhotoArray.length;
    }
    indexesCarouselPhotos.push(item);
    carouselItemsPhoto.append(carouselPhotoArray[item]);
  }
}

function handlerCarouselPhoto(e) {
  const element = e.target.closest('.item-foto');
  if (element) {
    if (element.classList.contains(active)) {
      return;
    }

    const step =
      indexesCarouselPhotos.indexOf(Number(element.dataset.sliderItem)) -
      countAddPhotoCarousel;

    handlerMoveCarousel(-step, step);
  }
}

function handlerMoveCarousel(count, step) {
  carouselItemsPhoto.style.transition = 'all ease 0.3s';
  carouselItemsPhoto.style.transform = `translateX(${
    valuePxDefault + valuePxMove * count
  }px)`;
  indexCarouselPhotoActive = indexesCarouselPhotos[countAddPhotoCarousel + step];
  clearActive(carouselPhotoArray);
  setActive(carouselPhotoArray[indexCarouselPhotoActive]);

  setTimeout(() => {
    carouselItemsPhoto.removeAttribute('style');
    setIndexesCarouselPhotos();
    setActiveContent(carouselPhotoArray[indexCarouselPhotoActive]);
  }, 350);
}

function setActiveContent(item) {
  const contentItem = getInfoConnectedContent(item);

  if (contentItem) {
    clearActive(sliderItems.children);
    setActive(contentItem);
  }
}

function getInfoConnectedContent(item) {
  for (let i of sliderItems.children) {
    if (i.dataset.sliderItem === item.dataset.sliderItem) {
      return i;
    }
  }
  return false;
}

prevBtn.addEventListener('click', () => handlerMoveCarousel(1, -1));
nextBtn.addEventListener('click', () => handlerMoveCarousel(-1, 1));
carouselItemsPhoto.addEventListener('click', handlerCarouselPhoto);

createSlider();

//#endregion section 'Feedback'
