"use strict";

// Завдання
const student = {
    name: '',
    lastName: '',
    table: {}
};

const stName = prompt("Enter your name");
const stLastName = prompt("Enter your last name");

student.name = stName;
student.lastName = stLastName;

const stTable = student.table;
let badMarks = 0;
let totalMarks = 0;

while (true) {
    let sbjName = prompt("Enter the subject name");

    if (sbjName === null) {
        break;
    }

    let sbjMark = prompt("Enter your mark");

    if (sbjMark === null) {
        break;
    }

    let sbjMarkNum = +sbjMark;

    stTable[sbjName] = sbjMarkNum;
    totalMarks += sbjMarkNum;

    if (sbjMarkNum < 4) {
        badMarks++;
    }
}

if (badMarks === 0 && Object.keys(stTable).length !== 0) {
    alert("Студент переведено на наступний курс");
}

const average = (totalMarks / Object.keys(stTable).length).toFixed(1);
if (average > 7) {
    alert("Студенту призначено стипендію");
}