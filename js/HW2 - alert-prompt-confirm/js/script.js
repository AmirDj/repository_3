"use strict";

//Теоретичні питання
/*
 №1

 "Number" - числовий тип даних. Всі числа до 2^53, а також "Infinity", "-Infinity" і "NaN".
 "BigInt" - числа більші, ніж 2^53.
 "Boolean" - логічний тип даних. Приймає і обробляє значення "true" та "false".
 "String" - рядок. Всі значення в лапках ("",'',`.....${...}`).
 "Null" - cпеціальне значення, яке є "нічим", "порожнім" або "невідомим значенням".
 "Undefined" - cпеціальне значення. Якщо змінна оголошена, але їй не надано жодного значення, то її значенням автоматично буде undefined.
 "Object" - використовується для зберігання колекцій даних або складніших об'єктів.
  "Symbol" - використовується для створення унікальних ідентифікаторів об'єктів.

 №2

 "===" - оператор "суворої рівностіі" перевіряє рівність без приведення типів, а "==" з приведенням.

 №3

 Оператор - це символ або ключове слово в програмуванні, яке вказує комп'ютеру виконати певну операцію. Оператори використовуються для виконання різних дій над операндами, які можуть бути змінними, константами, значеннями чи виразами.
*/

//Завдання

let userName;
let userAge;

userName = prompt("Enter your name");
if (userName.trim() === "") {
    userName = prompt("Please try again!", userName);
}

userAge = prompt("Enter your age");
if (isNaN(userAge)) {
    userAge = +prompt("Please try again!", userAge);
}

if (userAge < 18) {
  alert("You are not allowed to visit this website");
} else if (userAge >= 18 && userAge <= 22) {
  const confirmation = confirm("Are you sure you want to continue?");
  if (confirmation) {
    alert(`Welcome, ${userName}`);
  } else {
    alert("You are not allowed to visit this website");
  }
} else {
  alert(`Welcome, ${userName}`);
}
