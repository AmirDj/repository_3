"use strict";

//Завдання

const vehicles = [
    {
        name: 'Toyota Camry',
        description: 'A reliable sedan for everyday use',
        contentType: {
            name: 'Sedan',
        },
        locales: [
            {
                name: 'en_US',
                description: 'English description',
            },
            {
                name: 'fr_FR',
                description: 'Description en français',
            },
        ],
    },
    {
        name: 'Ford',
        description: 'A powerful pickup truck for heavy-duty tasks',
        contentType: {
            name: 'Truck',
        },
        locales: [
            {
                name: 'en_US',
                description: 'English description',
            },
            {
                name: 'es_ES',
                description: 'Descripción en español',
            },
        ],
    },
];

const filteredVehicles = filterCollection(
    vehicles,
    'fr_FR Ford',
    false,
    'name',
    'description',
    'contentType.name',
    'locales.name',
    'locales.description'
);

console.log(filteredVehicles);
