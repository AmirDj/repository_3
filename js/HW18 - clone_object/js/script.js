"use strict";

//Завдання

const obj1 = {
    boolean: true,
    num: 2,
    nan: NaN,
    date: new Date(),
    undefined: undefined,
    '': '',
    list: {
        null: null,
        string: "Hello",
        arr: [
            [1,2],[3,4],[5,6],
            [7,8],[9,10],[[]],
            ["str", null],[11,12],[13,-14]
        ],
        user: {
            name: "Jhon", 
            age: 31
        }
    }
}

function objClone(obj1) {
    if (obj1 instanceof Date) {
        return new Date(obj1.getTime()); 
    }

    if (typeof obj1 !== "object") {
        return obj1
    }

    let obj2 = Array.isArray(obj1) ? [] : {};

    for (const key in obj1) {
        const value = obj1[key];
        obj2[key] = objClone(value);
    }

    return obj2;
}

let clone = objClone(obj1);