import { useState } from 'react'
import './App.scss'
import Button from './components/Button/Button'
import ModalConstructor from './components/ModalConstructor';

function App() {
  const [imageModalState, setImageModal] = useState(false);
  const [textModalState, setTextModal] = useState(false);

  const openImageModal  = () => setImageModal(true);
  const openTextModal = () => setTextModal(true);

  const closeImageModal = () => setImageModal(false);
  const closeTextModal = () => setTextModal(false);

  return (
    <>
    <div className="button-wrapper">
      <Button type={'button'} classNames={'button'} onClick={openImageModal}>
        Open first modal
      </Button>
      <Button type={'button'} classNames={'button'} onClick={openTextModal}>
        Open second modal
      </Button>
    </div>
      
    {imageModalState && (
      <ModalConstructor 
        className={'modal__image'}
        title={'Product Delete!'}
        text={'By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted.'}
        onClose={closeImageModal}
        firstText = "NO, CANCEL"
        secondaryText ="YES, DELETE"
      />
    )}

    {textModalState &&(
      <ModalConstructor
        title={'Add Product “NAME”'}
        text={'Description for you product'}
        onClose={closeTextModal}
        firstText = "ADD TO FAVORITE"
      />
    )}
    </>
  )
}

export default App
