import './Modal.scss'

export default function ModalHeader({children}) {
    return(
        <div className="modal__header">
            {children}
        </div>
    )
}