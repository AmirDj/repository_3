import './Modal.scss'
import '../Button/Button.scss'

export default function ModalFooter({firstText, firstClick, secondaryText, secondaryClick}) {
    return(
        <div className='modal__footer'>
            {firstText && <button onClick={firstClick}>{firstText}</button>}
            {secondaryText && <button onClick={secondaryClick}>{secondaryText}</button>}
        </div>
    )
}