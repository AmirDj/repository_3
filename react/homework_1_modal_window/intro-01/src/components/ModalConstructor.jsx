import ModalWrapper from './Modal/ModalWrapper'
import Modal from './Modal/Modal'
import ModalClose from './Modal/ModalClose'
import ModalHeader from "./Modal/ModalHeader"
import ModalBody from "./Modal/ModalBody"
import ModalFooter from "./Modal/ModalFooter"

export default function ModalConstructor({className, title, text, onClose, firstText, secondaryText}) {
    return(
        <>
            <ModalWrapper onClose={onClose}>
                <Modal>
                    <ModalClose onClick={onClose} />
                    <ModalHeader>
                        <div className={className}></div>
                    </ModalHeader>
                    <ModalBody>
                        <h1>{title}</h1>
                        <p>{text}</p>
                    </ModalBody>
                    <ModalFooter 
                        firstText={firstText}
                        secondaryText={secondaryText}
                        firstClick={onClose}
                        secondaryClick ={onClose}
                    />
                </Modal>
            </ModalWrapper>
        </>
    )
}
