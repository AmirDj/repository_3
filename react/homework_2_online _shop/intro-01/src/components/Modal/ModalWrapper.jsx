import './Modal.scss'

export default function ModalWrapper({children, onClose}) {

    function clickOnWrap(event) {
        if (event.target === event.currentTarget) {
            onClose()
        } 
    }

    return(
        <div className="modal-wrapper" onClick={clickOnWrap}>
            {children}
        </div>
    )
}