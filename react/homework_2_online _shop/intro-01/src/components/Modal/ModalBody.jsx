import './Modal.scss'

export default function ModalBody({children}) {
    return(
        <div className='modal__body'>
            {children}
        </div>
    )
}