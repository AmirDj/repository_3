import ModalWrapper from './ModalWrapper'
import Modal from './Modal'
import ModalClose from './ModalClose'
import ModalHeader from "./ModalHeader"
import ModalBody from "./ModalBody"
import ModalFooter from "./ModalFooter"
import PropTypes from "prop-types";

export default function ModalConstructor({className, title, text, onClose, firstText, secondaryText}) {
    return(
        <>
            <ModalWrapper onClose={onClose}>
                <Modal>
                    <ModalClose onClick={onClose} />
                    <ModalHeader>
                        <div className={className}></div>
                    </ModalHeader>
                    <ModalBody>
                        <h1>Add "{title}" to cart?</h1>
                        <p>Color: {text}</p>
                    </ModalBody>
                    <ModalFooter 
                        firstText={firstText}
                        secondaryText={secondaryText}
                        firstClick={onClose}
                        secondaryClick ={onClose}
                    />
                </Modal>
            </ModalWrapper>
        </>
    )
}