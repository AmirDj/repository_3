import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStar } from "@fortawesome/free-solid-svg-icons";
import { faCartShopping } from "@fortawesome/free-solid-svg-icons";
import './Header.scss'

export default function Header({ cartItemTotal, favoriteItemTotal }) {

    return (
        <header className='header'>
            <div className="header-wrapper container">
                <h1 className='logo'>TechShop</h1>
                <div className="header__icons">
                    <div
                        className="header__icons--wrapper"
                    >
                        <FontAwesomeIcon
                            icon={faCartShopping}
                            size="lg"
                        />
                        <span>{cartItemTotal}</span>
                    </div>
                    <div
                        className="header__icons--wrapper"
                    >
                        <FontAwesomeIcon
                            icon={faStar}
                            size="lg"
                        />
                        <span>{favoriteItemTotal}</span>
                    </div>
                </div>
            </div>
        </header>
    )
}