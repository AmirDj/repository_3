import Button from '../Button/Button'
import PropTypes from "prop-types";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import { faStar } from "@fortawesome/free-solid-svg-icons";
import './ProductsCard.scss'


export default function ProductsCard({inCart, inFavorites, product, showModal, addToCart, addToFavorites}) {
    const {name, price, image} = product;

    const handleClickAddToCart = () => {
        showModal(product);
        addToCart(product);
    };

    const handleClickAddToFavorites = () => {
        addToFavorites(product);
    };

    return(
        <div className="productCard">
            <div className="productCard__header">
                {!inFavorites && (
                    <Button
                        classNames="productCard__favorite"
                        onClick={handleClickAddToFavorites}
                    >
                        <FontAwesomeIcon
                            icon={faStar}
                            size="lg"
                            style={{ color: "grey" }}
                        />
                    </Button>
                )}
                {inFavorites && (
                    <Button
                        classNames="productCard__favorite"
                        style={{
                            backgroundColor: "#dddddd",
                            borderRadius: "60px",
                        }}
                        onClick={handleClickAddToFavorites}
                    >
                        <FontAwesomeIcon
                            icon={faStar}
                            size="lg"
                            style={{ color: "#ffa900" }}
                        />
                    </Button>
                )}
                <img src={image} alt={name} />
            </div>
            <div className="productCard__body">
                <h3 className="productCard__name">{name}</h3>
                <p className="productCard__price">
                    {price}
                    <span>₴</span>
                </p>
            </div>
            <div className="productCard__footer">
                {!inCart && (
                    <Button classNames="productCard__footer--addToCart" onClick={ handleClickAddToCart}>
                        Add to cart
                    </Button>
                )}
                {inCart && (
                    <Button
                        classNames="productCard__footer--inCart"
                    >
                        Product in cart
                    </Button>
                )} 
            </div>
        </div>
    )
}

ProductsCard.propTypes = {
    product: PropTypes.shape({
        name: PropTypes.string,
        price: PropTypes.number,
        image: PropTypes.string,
    }),
    inCart: PropTypes.bool,
    inFavorites: PropTypes.bool,
    AddToCart: PropTypes.func,
    AddToFavorites: PropTypes.func,
};
