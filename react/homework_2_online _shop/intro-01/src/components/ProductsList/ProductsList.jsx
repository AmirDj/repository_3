import ProductsCard from '../ProductsCard/ProductsCard'
import PropTypes from "prop-types";
import './ProductsList.scss'

export default function ProductsList({products, cartItems, favoriteItems, showModal, addToCart, addToFavorites}) {
    
    return(
        <div className="container">
            <section className='products'>
                <div className="products__cards">
                    {
                        products.map((product) => (
                            <ProductsCard 
                            key={product.id}
                            product={product}
                            inCart={cartItems.some((item) => item.id === product.id)}
                            inFavorites={favoriteItems.some((item) => item.id === product.id)}
                            addToCart={addToCart}
                            addToFavorites={addToFavorites}
                            showModal={showModal}

                            />
                        ))
                    }
                </div>
            </section>
        </div>
    )
}

ProductsList.propTypes = {
    products: PropTypes.array,
    cartItems: PropTypes.array,
    favoriteItems: PropTypes.array,
    addToCart: PropTypes.func,
    addToFavorites: PropTypes.func,
    showModal: PropTypes.func,
};
