import { useState, useEffect } from "react";
import PropTypes from "prop-types";
import '../Modal/Modal.scss'
import ModalConstructor from '../Modal/ModalConstructor'
import Header from "../Header/Header";
import ProductsList from "../ProductsList/ProductsList";

export default function Home() {

    const [products, setProducts] = useState([]);

    const [cartItems, setCartItems] = useState(
        JSON.parse(localStorage.getItem("cartsItems")) || []
    );

    const [favoriteItems, setFavoriteItems] = useState(
        JSON.parse(localStorage.getItem("favoriteItems")) || []
    );

    const [textModalState, setTextModal] = useState(false);

    const openTextModal = () => {
        setTextModal(true);
    }

    const closeTextModal = () => {
        const modal = document.querySelector('.modal-wrapper');
        modal.style.display = "none";
        setTextModal(false);
    }

    const sendRequest = async (url) => {
        const response = await fetch(url);
        const products = await response.json();
        return products;
    };

    useEffect(() => {
        sendRequest("CardsData.json").then((products) => {
            console.log(products);
            setProducts(products);
        });
    }, []);

    function handleAddToCart(product) {
        const indexItem = cartItems.findIndex((item) => item.id === product.id);

        if (indexItem === -1) {
            const newItem = {
                id: product.id,
                name: product.name,
                price: product.price,
                image: product.image,
                color: product.color,
                acticle: product.acticle,
                quantity: 1,
            };
            const mergedCartItems = [...cartItems, newItem];
            setCartItems(mergedCartItems);
            localStorage.setItem("cartsItems", JSON.stringify(mergedCartItems));

        } else {
            const updatedItem = {
                ...cartItems[indexItem],
                quantity: cartItems[indexItem].quantity + 1,
            };
            const updatedCartItems = [...cartItems];
            updatedCartItems.splice(indexItem, 1, updatedItem);

            setCartItems(updatedCartItems);
            localStorage.setItem("cartsItems", JSON.stringify(updatedCartItems));

        }
    }

    function handleAddToFavorites(product) {
        const indexItem = favoriteItems.findIndex(
            (item) => item.id === product.id
        );

        if (indexItem === -1) {
            const newItem = {
                id: product.id,
                name: product.name,
                price: product.price,
                image: product.image
            };

            const mergedFavoriteItems = [...favoriteItems, newItem];

            setFavoriteItems(mergedFavoriteItems);
            localStorage.setItem("favoriteItems", JSON.stringify(mergedFavoriteItems));
        } else {
            const updatedFavoriteItems = favoriteItems.filter(
                (item) => item.id !== product.id
            );
            setFavoriteItems(updatedFavoriteItems);
            localStorage.setItem("favoriteItems", JSON.stringify(updatedFavoriteItems));
        }
    }

    const cartItemTotal = cartItems.reduce(
        (total, item) => total + item.quantity,
        0
    );

    const favoriteItemTotal = favoriteItems.length;

    return (
        <>
            <Header
                cartItemTotal={cartItemTotal}
                favoriteItemTotal={favoriteItemTotal}
            />

            <main>
                <ProductsList
                    products={products}
                    cartItems={cartItems}
                    favoriteItems={favoriteItems}
                    showModal={openTextModal}
                    addToCart={handleAddToCart}
                    addToFavorites={handleAddToFavorites}
                />
            </main>

            {textModalState &&(
              cartItems.map((product)=>(
                <ModalConstructor
                key={product.id}
                title={product.name}
                text={product.color}
                firstText={'Add to cart'}
                onClose={closeTextModal} 
                />
              ))
            )}
        </>
    );
}

ModalConstructor.defaultProps = {
    text: 'Not mentioned'
};

Home.propTypes = {
    products: PropTypes.arrayOf(PropTypes.object),
    cartItems: PropTypes.arrayOf(PropTypes.object),
    favoriteItems: PropTypes.arrayOf(PropTypes.object),
    addToCart: PropTypes.func,
    addToFavorites: PropTypes.func,
    showModal: PropTypes.func,
    cartItemTotal:PropTypes.number,
    favoriteItemTotal:PropTypes.number,
    key:PropTypes.number,
    title:PropTypes.string,
    text:PropTypes.string,
    firstText:PropTypes.string,
    onClose:PropTypes.func
};