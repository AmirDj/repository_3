"use strict";

let users = 'https://ajax.test-danit.com/api/json/users';
let posts = 'https://ajax.test-danit.com/api/json/posts';

class Card{
    constructor(title, text, user){
        this.title = title;
        this.text = text;
        this.user = user;
        this.id = null;
        this.card = null;
    }

    render(){
        const card = document.createElement('div');
        card.classList.add('card');

        const cardUser = `<h3 class="card-user">${this.user.name}</h3>`;   
        const cardUserName = `<p class="card-username">${this.user.username} <a href="mailto: ${this.user.email}">${this.user.email}</a></p>`;
        const cardTitle = `<p class="card-title">${this.title}</p>`;
        const cardText = `<p class="card-text">${this.text}</p>`;
        const deleteButton = `<button class="delete-button">Delete</button>`;

        card.innerHTML = cardUser + cardUserName + cardTitle + cardText + deleteButton;

        card.querySelector(".delete-button").addEventListener("click", () => {
            this.delete();
        });
        this.card = card;
        return card
    }

    delete(){
        if(this.id){
            fetch(posts + `/${this.id}`, {
                method: "DELETE",
            })
                .then(response => {
                    if (response.ok) {
                        this.card.remove();
                    } else {
                        console.log("Error when deleting");
                    }
                })
                .catch(error => {
                    console.error(error.message);
                });
        }
    }
}

fetch(users)
    .then(response => response.json())
    .then(users => {
        fetch(posts)
            .then(response => response.json())
            .then(posts => {
                const cardsContainer = document.createElement('div');
                cardsContainer.classList.add('cards-container');
                document.body.append(cardsContainer);
                posts.forEach(post => {
                    const user = users.find((user) => user.id === post.userId);
                    const card = new Card(post.title, post.body, user);
                    card.id = post.id;
                    cardsContainer.appendChild(card.render());
                });
            })
            .catch(error => {
                console.error(error.message);
            })
    })
    .catch(error => {
        console.error(error.message);
    })