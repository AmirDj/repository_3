"use strict";

const urlUserIP  = 'https://api.ipify.org/?format=json';

const containerElement = document.querySelector('.container');
const IPButton = document.querySelector('#findIPButton');

async function getUsersIP() {
    try {
        const response = await fetch(urlUserIP)
        const data = await response.json()
        return data;
    } catch (error) {
        console.error(error.message);
    }
}

async function getUserLocation(dataIP) {
    try {
        const response = await fetch(`http://ip-api.com/json/${dataIP}?fields=1622041`)
        const data = await response.json()
        return data;
    } catch (error) {
        console.error(error.message);
    }
}

async function getInfo() {
    try {
        const {ip} = await getUsersIP()
        const location = await getUserLocation(ip)
        const card = new Card(location);
        containerElement.appendChild(card.render());
    } catch (error) {
        console.error(error.message);
    }
}

class Card{
    constructor({continent, country, regionName, city, district}){
        this.continent = continent;
        this.country = country;
        this.regionName = regionName;
        this.city = city;
        this.district = district;
    }

    render(){
        const cardDiv = document.createElement('ul');
        cardDiv.classList.add('location-info');
        cardDiv.innerHTML = `
        <h3 class="location-info__title">Your location is:</h3>
        <li class="location-info__text">Continent: ${this.continent}</li>
        <li class="location-info__text">Country: ${this.country}</li>
        <li class="location-info__text">Region: ${this.regionName}</li>
        <li class="location-info__text">City: ${this.city}</li>
        <li class="location-info__text">District: ${this.district}</li>
        `
        return cardDiv;
    }
}

IPButton.addEventListener('click', getInfo, {once: true})