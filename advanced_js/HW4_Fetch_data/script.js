"use strict";

const urlApi = "https://ajax.test-danit.com/api/swapi/films";

function fetchData(url) {
  return fetch(url)
    .then((response) => {
      if (response.ok) {
        return response.json();
      } else {
        throw new Error("Network Error");
      }
    })
    .then((data) => {
      return data;
    })
    .catch((error) => {
      console.error(error);
    });
}

function renderFilmName(data) {
  return data.map(({ episodeId, name }) => {
    const filmElement = document.createElement("h3");
    filmElement.innerHTML = `Епізод ${episodeId}: ${name}`;
    return filmElement;
  });
}

function episodeDescription(data) {
  return data.map(({ openingCrawl }) => {
    const element = document.createElement("p");
    element.innerHTML = `<span style="font-weight: 600">Короткий опис:</span> ${openingCrawl}`;
    return element;
  });
}

function renderCharacters(data) {
  const array = data.map(({ characters }) => {
    const charList = document.createElement("ul");

    characters.map((items) => {
      fetchData(items).then((data) => {
        const charListElement = document.createElement("li");
        charListElement.textContent = data.name;

        charList.append(charListElement);
      });
    });

    return charList;
  });

  return array;
}

function renderAll(url) {
  const container = document.querySelector("#root");
  const filmList = document.createElement("ul");
  filmList.style.listStyle = "none";
  container.append(filmList);

  fetchData(url).then((data) => {
    for (let i = 0; i < data.length; i++) {
      const filmListElement = document.createElement("li");

      filmListElement.append(renderFilmName(data)[i]);
      filmListElement.append(episodeDescription(data)[i]);
      filmListElement.append(renderCharacters(data)[i]);
      filmList.append(filmListElement);
    }
  });
}

function renderCharactersAll(url) {
  fetchData(url).then((data) => renderCharacters(data));
}

renderAll(urlApi);
