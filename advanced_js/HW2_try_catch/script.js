"use strict";

const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
];

function showBookInfo(books) {
  const rootElement = document.getElementById('root');
  const ul = document.createElement('ul');

  books.forEach(book => {
      const errors = {};
      const propertiesToCheck = ['author', 'name', 'price'];

      propertiesToCheck.forEach(property => {
          if (!(property in book)) {
              errors[property] = `There is no '${property}' property in: ${JSON.stringify(book)} object`;
          }
      });

      try {
          if (Object.keys(errors).length > 0) {
              throw errors;
          }

          const li = document.createElement('li');
          li.textContent = `Author: ${book.author}, Name: ${book.name}, Price: ${book.price}`;
          ul.appendChild(li);
      } catch (error) {
          for (const key in error) {
              console.error(error[key]);
          }
      }
  });

  rootElement.appendChild(ul);
}


showBookInfo(books);