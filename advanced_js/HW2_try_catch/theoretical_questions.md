Конструкцію try...catch використовують для обробки помилок та винятків під час виконання певного коду.

1. Під час роботи з JSON:
Операції парсингу та розбору JSON даних можуть викликати помилки, особливо якщо дані некоректні або несумісні зі структурою JSON.

const invalidJSON = '{ "name": "John", "age": }';

try {
    const parsedData = JSON.parse(invalidJSON);
    console.log('Parsed data:', parsedData);
} catch (error) {
    console.error('Error when parsing JSON:', error);
};

2. Під час взаємодії з DOM:
Робота з DOM може викликати помилки, особливо при спробі отримати доступ до неіснуючого елемента або викликати неправильні методи.

const element = document.getElementById('nonexistentElement');

try {
    element.classList.add('active');
    console.log('Element is active');
} catch (error) {
    console.error('Error occurred while working with DOM:', error);
};
