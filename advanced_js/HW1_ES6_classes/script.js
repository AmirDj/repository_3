class Employee{
    constructor(name, age, salary){
        this.nameVal = name;
        this.ageVal = age;
        this.salaryVal = salary;
    }

    get name(){
        return this.nameVal;
    }

    set name(value){
        this.nameVal = value;
    }

    get age(){
        return this.ageVal;
    }

    set age(value){
        this.ageVal = value;
    }

    get salary(){
        return this.salaryVal;
    }

    set salary(value){
        this.salaryVal = value;
    }
}

class Programmer extends Employee{
    constructor(name, age, salary, lang){
        super(name, age, salary);
        this.lang = lang;
    }

    get salary(){
        return super.salary * 3;
    }
}

const firstDev = new Programmer('Jhon', 27, 1500, ['JS', 'Java','C++']);
console.log(firstDev);

const secondDev = new Programmer('Jack', 35, 3000, ['Assembly', 'Python', 'Ruby']);
console.log(secondDev);

const thirdDev = new Programmer('Tom', 19, 500, ['Rust', 'Nim', 'Lua']);
console.log(thirdDev);