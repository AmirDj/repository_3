const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const browserSync = require('browser-sync').create();
const jsMinify = require('gulp-js-minify');
const uglify = require('gulp-uglify');
const cleanCSS = require('gulp-clean-css');
const clean = require('gulp-clean');
const concat = require('gulp-concat');
const imagemin = require('gulp-imagemin');
const autoprefixer = require('gulp-autoprefixer');

gulp.task('clean', function (cb) {
  return gulp.src('./dist', { read: false, allowEmpty: true })
    .pipe(clean(cb));
});

gulp.task('styles', function () {
  return gulp.src('./src/scss/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(cleanCSS())
    .pipe(concat('styles.min.css'))
    .pipe(gulp.dest('./dist/css'))
    .pipe(browserSync.stream());

});

gulp.task('scripts', function () {
  return gulp.src('./src/js/**/*.js')
    .pipe(uglify())
    .pipe(concat('scripts.min.js'))
    .pipe(gulp.dest('./dist/js'))
    .pipe(browserSync.stream());

});

gulp.task('images', function () {
  return gulp.src('./src/img/**/*')
    .pipe(imagemin())
    .pipe(gulp.dest('./dist/img'));
});

gulp.task('server', function () {
  browserSync.init({
    server: {
      baseDir: "./",
    },
    port: 3000,
    open: true,
  });

  gulp.watch('./src/scss/**/*.scss', gulp.series('styles'));
  gulp.watch('./src/js/*.js', gulp.series('scripts'));
});

gulp.task('build', gulp.series('clean', gulp.parallel('styles', 'scripts', 'images')));
gulp.task('dev', gulp.series('server'));
gulp.task('default', gulp.series('dev'));
